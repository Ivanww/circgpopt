
import numpy as np
from GPyOpt.acquisitions.EI import AcquisitionEI
from GPyOpt.util.general import get_quantiles

from Utils import initLogger, feasible
class AcquisitionEIWithTau(AcquisitionEI):
    '''This class calculate EI use Tau if it is set. Or act as normal EI '''
    def __init__(self, model, space, optimizer=None, cost_withGradients=None, jitter=0.01, logger=None):
        super(AcquisitionEIWithTau, self).__init__(model, space, optimizer, cost_withGradients=cost_withGradients, jitter=jitter)

        self.tau = None
        self.logger = logger
        if not self.logger:
            self.logger = initLogger(self.__class__.__name__)

    def setTau(self, tau):
        '''Set Tau as tau'''
        self.tau = tau
        self.logger.info('set tau as %f', self.tau)

    def updateTau(self, Y):
        '''Find feasible Y and set tau'''
        self.logger.debug('updating Tau...')
        fmin = None
        YY = np.atleast_2d(Y)
        Y_feasible = YY[feasible(YY)]
        if Y_feasible.size > 0:
            fmin = Y_feasible[:,0].min()

        if fmin:
            if not self.tau:
                self.setTau(fmin)
            else:
                self.setTau(min((self.tau, fmin)))
        self.logger.debug('update Tau done!')


    def _compute_acq(self, x):
        if not self.tau:
            return super(AcquisitionEIWithTau, self)._compute_acq(x)
        else:
            m, s = self.model.predict(x)
            phi, Phi, _ = get_quantiles(self.jitter, self.tau, m, s)
            f_acqu = (self.tau - m + self.jitter) * Phi + s * phi
            return f_acqu

    def _compute_acq_withGradients(self, x):
        if not self.tau:
            f_acqu, df_acqu = super(AcquisitionEIWithTau, self)._compute_acq_withGradients(x)
        else:
            m, s, dmdx, dsdx = self.model.predict_withGradients(x)
            phi, Phi, _ = get_quantiles(self.jitter, self.tau, m, s)
            u = (self.tau - m + self.jitter) / s
            dudx = (-s * dmdx - (self.tau - m + self.jitter) * dsdx) / s**2
            f_acqu = s * (u * Phi + phi)
            df_acqu = dsdx * (u * Phi + phi) + s * Phi * dudx
            # f_acqu = (self.tau - m + self.jitter) * Phi + s * phi
            # df_acqu = dsdx * phi - Phi * dmdx

        return f_acqu, df_acqu

if __name__ == '__main__':
    pass
