
import GPyOpt
import GPy
import numpy as np

import ConfigLoader as  CL
from SPObjective import SpiceObjective
from ConstrainedLogWeightedEI import ConstrainedLogWeightedEI
from EpsilonGreedyByOpt import EpsilonGreedyBayesianOptimization
from MSPOptimizer import MixSamplingOptimizer
from ObjectiveDecorator import NormalizedObjective, InvalidResultFilter
from Utils import regularSpace

if __name__ == '__main__':
    # load config
    config = CL.ConfigLoader('singleEndedConf.json')
    # config = CL.ConfigLoader('twoStage.json')

    # regular input to [-1, 1]
    space = GPyOpt.Design_space(space = config.space)
    regularS = GPyOpt.Design_space(space=regularSpace(config.space))


    # GP model for target
    mainKernel = GPy.kern.RBF(config.para_num, variance=1., ARD=True)#GPy.kern.Bias(config.para_num)
    mainModel = GPyOpt.models.GPModel(
        kernel=mainKernel,
        noise_var=None,
        exact_feval=False,
        normalize_Y=False, # True
        optimizer='bfgs',
        max_iters=1000,
        optimize_restarts=1,
        sparse=False,
        num_inducing=10,
        verbose=False # False
    )

    # Acquisition Function Optimizer
    acqOptimizer = MixSamplingOptimizer(regularS, optimizer='slsqp', n_samples=60, logger=config.logger)

    # initial X
    initialDesign = GPyOpt.util.stats.initial_design(config.init_sample_method, regularS, config.init_sample_size)
    # initialDesign = np.loadtxt('feasInput.txt', dtype='float64', delimiter=',')

    # GP models for constrains
    constrainModels = []
    for i in range(config.constrainNum):
        constrainModels.append(
            GPyOpt.models.GPModel(
                kernel=GPy.kern.RBF(config.para_num, ARD=True),#GPy.kern.Bias(config.para_num),
                # kernel=None,
                noise_var=None,
                exact_feval=False,
                normalize_Y=False,
                optimizer='bfgs',
                max_iters=1000,
                optimize_restarts=1,
                sparse=False,
                num_inducing=10,
                verbose=False
            )
        )
    # Acquistion Function
    acquisition = ConstrainedLogWeightedEI(
        model=mainModel,
        space=regularS,
        # space=space,
        constrain_models=constrainModels,
        optimizer=acqOptimizer,
        cost_withGradients=None,
        jitter=0.01,
        logger=config.logger,
    )

    # Wrapper for Acquisition Function
    evaluator = GPyOpt.core.evaluators.Sequential(acquisition)

    # SPice Objective : Problem
    # objective = SpiceObjective(config)
    objective = InvalidResultFilter(regularS, NormalizedObjective(space, SpiceObjective(config)))

    # Bayesian Optimization Manager
    bo = EpsilonGreedyBayesianOptimization(
        model=mainModel,
        # space=space,
        space=regularS,
        objective=objective,
        acquisition=acquisition,
        evaluator=evaluator,
        X_init=initialDesign,
        Y_init=None,
        cost=None,
        normalize_Y=False,
        model_update_interval=1,
        epsilon=0.05,
        logger=config.logger
    )

    # Run Optimization
    try:
        bo.run_optimization(
            max_iter=1000,
            max_time=config.max_time,
            eps=config.opt_eps,
            verbosity = True,
            save_models_parameters=config.save_models_parameters,
            report_file=config.report_file,
            evaluations_file=config.evaluations_file,
            models_file=config.models_file
        )
    except:
        print('Dumping data...')
        np.savetxt('core_dump_X.txt', bo.X, delimiter=',')
        np.savetxt('core_dump_Y.txt', bo.Y, delimiter=',')
        print('Dump data done, exiting...')
        exit(1)
    # Report Result
    print(bo.fx_opt)
    print(bo.x_opt)