
import numpy as np
import GPy
from GPyOpt.models.gpmodel import GPModel

from AcquisitionEIWithTau import AcquisitionEIWithTau
# yxw173630
from Utils import reportModel
class EIWithConstrainModels(AcquisitionEIWithTau):
    '''
    This class has extra models to modeling the constrains
    And constrain models are indenpendent from each other
    Note: constrain models should be GPyOpt.models
    '''
    def __init__(self, model, space, constrain_models, optimizer=None, cost_withGradients=None, jitter=0.01, logger=None):
        super(EIWithConstrainModels, self).__init__(model, space, optimizer,
            cost_withGradients=cost_withGradients, jitter=jitter, logger=logger)
        self.constrNum = len(constrain_models)
        self.constrainModels = constrain_models

    def updateConstrainModel(self, X, Y, X_new, Y_new):
        '''
        update the constrain models according to new observation
        Note: main model is not update here!!
        '''
        # updateModel will ignore last two arguments. So make sure X, Y contain all data (old+new)
        assert(len(self.constrainModels) != 0)
        self.logger.info('updating constrain models')

        i = 0
        while True:
            if i == self.constrNum:
                break
            try:
                self.constrainModels[i].updateModel(X, Y[:,i+1].reshape(Y.shape[0], 1), None, None)
            except np.linalg.linalg.LinAlgError:
                # if fail try to rebuild the model
                self.logger.warning('[Updating model %d fail, rebuilding...]' % (i))
                self.constrainModels[i] = GPModel(
                    kernel=GPy.kern.RBF(X.shape[0], ARD=True),
                    noise_var=None,
                    exact_feval=False,
                    normalize_Y=False, # True
                    optimizer='bfgs',
                    max_iters=1000,
                    optimize_restarts=1,
                    sparse=False,
                    verbose=False # False
                )
                continue
            i += 1

        # TODO: Can only check Y_new
        # self.updateTau(Y)
