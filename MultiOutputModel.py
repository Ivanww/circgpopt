
import GPy
import numpy as np
import numpy.matlib as mt

from Utils import bestXY
def initTestDataSmallWithBest():
    # load data
    initX = np.loadtxt('testX.txt', dtype='float64', delimiter=',')
    trainX = initX[:50,:]
    testX = np.atleast_2d(initX[-2:,:])
    initY = np.loadtxt('testY.txt', dtype='float64', delimiter=',')
    trainY = initY[:50,:]
    testY = initY[-2:,:]
    bestX, bestY = bestXY(initX, initY)
    # distribute input
    dim = initX.shape[1]
    outputNum = initY.shape[1]
    index = [np.random.choice(trainX.shape[0], 25, replace=False) for i in range(outputNum)]
    xList = [trainX[i] for i in index]
    yList = [trainY[index[i], i].reshape(25, 1) for i in range(outputNum)]
    return xList, testX, yList, testY, bestX, bestY


def initTestData():
    # load data
    initX = np.loadtxt('testX.txt', dtype='float64', delimiter=',')
    trainX = initX[:-1,:]
    testX = np.atleast_2d(initX[-1,:])
    initY = np.loadtxt('testY.txt', dtype='float64', delimiter=',')
    trainY = initY[:-1,:]
    testY = initY[-1,:]
    # distribute input
    dim = initX.shape[1]
    outputNum = initY.shape[1]
    index = [np.random.choice(trainX.shape[0], 150, replace=False) for i in range(outputNum)]
    xList = [trainX[i] for i in index]
    yList = [trainY[index[i], i].reshape(150, 1) for i in range(outputNum)]
    return xList, testX, yList, testY

def buildTestModel(xList, yList):
    # define kernels
    dim = xList[0].shape[1]
    out = len(yList)
    kern0 = GPy.kern.Bias(input_dim=dim)
    kern1 = GPy.kern.RBF(input_dim=dim, ARD=True)
    K = GPy.util.multioutput.LCM(input_dim=dim, num_outputs=out, kernels_list=[kern0, kern1])
    # build model
    model = GPy.models.GPCoregionalizedRegression(xList, yList, kernel=K)
    model['.*ICM.*var'].unconstrain()
    model['.*ICM0.*var'].constrain_fixed(1.)
    model['.*ICM0.*W'].constrain_fixed(0)
    return model

def testModelAndEI():
    xList, testX, yList, testY, bestX, bestY = initTestDataSmallWithBest()
    # build kernel
    dim = xList[0].shape[1]
    out = len(yList)
    print(dim, out)
    kern0 = GPy.kern.Bias(input_dim=dim)
    kern1 = GPy.kern.RBF(input_dim=dim, ARD=True)
    K = GPy.util.multioutput.LCM(input_dim=dim, num_outputs=out, kernels_list=[kern0, kern1])
    # build model
    print('[initial model...]')
    from CoregionalizedGPModel import CoregionalizedGPModel
    model = CoregionalizedGPModel(out, K)
    # build EI
    import GPyOpt
    from CoregionalizedEI import CoregionalizedEI
    from ConfigLoader import ConfigLoader
    from MSPOptimizer import MixSamplingOptimizer
    config = ConfigLoader('singleEndedConf.json')
    space = GPyOpt.Design_space(space = config.space)
    optimizer = MixSamplingOptimizer(space, 'slsqp', 60)
    print('[building acq...]')
    acq = CoregionalizedEI(model, space, out, optimizer)
    # optimize model
    print('[updating data...]')
    model.updateModel(xList, yList, None, None)
    print('[update data done]')
    # get acq and gradients
    print('[testX Shape] %s' % (str(testX.shape)))
    wEI = acq.acquisition_function(testX)
    print('[ACQ SHAPE][%s]' % (str(wEI.shape)))
    print(wEI)
    EI, dEIdx = acq.acquisition_function_withGradients(testX)
    print('[EI SHAPE][%s]' % (str(EI.shape)))
    print(EI)
    print('[dEIdx SHAPE][%s]' % (str(dEIdx.shape)))
    print(dEIdx)

    optimizer.updateBest(bestX)
    print(acq.optimize())
    # xListNew, testX, yListNew, testY = initTestDataSmall()
    # model.updateModel(xListNew, yListNew, None, None)

def expandX(x, output_num):
    assert(np.atleast_2d(x).shape[0] == 1)
    index = np.arange(output_num).reshape((output_num, 1))
    Xs = mt.repmat(x, output_num, 1)
    return np.hstack([Xs, index])

def fitMultiOutputModel():
    # load data
    xList, testX, yList, testY = initTestData()
    # build model
    model = buildTestModel(xList, yList)
    # fitting
    model.optimize()
    # report
    print(model)
    # predict
    dim = xList[0].shape[1]
    out = yList[0].shape[1]
    newX = expandX(testX, out) # predict all outputs
    noise_dict = {'output_index':newX[:, dim:].astype(int)}

    m, s = model.predict(newX, Y_metadata=noise_dict)
    dmdx, dvdx = model.predictive_gradients(newX)
    print(m.shape)
    print(s.shape)
    print(dmdx.shape)
    print(dvdx.shape)
    print(m)
    print(s)
    # dmdx, dvdx = model.predictive_gradients(newX)
    # print(dmdx.shape)
    # print(dvdx.shape)
    print(testY)
if __name__ == '__main__':
    # fitMultiOutputModel()
    testModelAndEI()
