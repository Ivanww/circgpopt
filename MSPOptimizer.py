
import numpy as np
from GPyOpt.optimization.acquisition_optimizer import ContAcqOptimizer
from GPyOpt.util.general import samples_multidimensional_uniform

from SlsqpOptimizer import Opt_slsqp
from Utils import initLogger

class ReferenceOptimizer(ContAcqOptimizer):
    '''
    This class keep a record of best result for further optimization
    '''
    def __init__(self, space, optimizer='lbfgs', n_samples=5000, fast=False, random=True, search=True, logger=None, **kwargs):
        super(ReferenceOptimizer, self).__init__(space, optimizer='lbfgs', n_samples=n_samples, fast=fast, random=random, search=search, **kwargs)
        self.best = None

    def updateBest(self, X):
        '''set the reference location to sampling locally'''
        self.best = X

class MixSamplingOptimizer(ReferenceOptimizer):
    '''
    This class mix sampls from beat location and global space
    Then try to opt them
    Note : slssqp supported
    '''
    def __init__(self, space, optimizer='lbfgs', n_samples=5000, fast=False, random=True, search=True, logger=None, **kwargs):
        super(MixSamplingOptimizer, self).__init__(space, optimizer='lbfgs', n_samples=n_samples, fast=fast, random=random, search=search, **kwargs)
        self.dim = len(self.bounds)
        self.bounds = np.asarray(self.bounds)
        # self.sample_s = 0.005 # variance when sampling locally
        self.sample_s = np.empty((self.dim,))
        self.sample_s.fill(0.005)
        self.globalSampleNum = min(self.n_samples * 100, 1e5)

        self.logger = logger
        if not self.logger:
            self.logger = initLogger(self.__class__.__name__)

        self.lSampleNum = int(self.n_samples / 5)
        self.gSampleNum = self.n_samples - self.lSampleNum

        # support sqp opt method
        if optimizer == 'slsqp':
            self.optimizer = Opt_slsqp(space=self.space)

        # Note: self.samples will not be use directly

        self.logger.info('[Total Samp][Glob Trail Samp][Opt Method]')
        self.logger.info('[%d][%d][%s]' % (self.n_samples, self.globalSampleNum, optimizer))

    def _mixSamples(self, f):
        '''
        Mix up global samples with local samples
        '''
        assert(self.best is not None)

        # sampling around best result
        localSamples = np.random.normal(
            loc=self.best.reshape((self.dim,)),
            scale=self.sample_s,
            size=(self.lSampleNum, self.dim)
        ).clip(
            min=self.bounds[:,0],
            max=self.bounds[:,1]
        )
        self.samples[:self.lSampleNum, :] = localSamples
        # add best global samples
        globalSamples = np.random.uniform(low=self.bounds[:, 0], high=self.bounds[:, 1], size=(self.globalSampleNum, self.dim))
        self.logger.info('[SHAPE OF GLOBAL SAMPLE] %s' % (str(globalSamples.shape)))

        pred = f(globalSamples)

        self.samples[self.lSampleNum:, :] = globalSamples[pred[:,0].argsort()[:self.gSampleNum], :]

    def optimize(self, f=None, df=None, f_df=None):
        if f is None and f_df is not None:
            def _f(x):
                y, dy = f_df(x)
                return y
        else:
            _f = f

        self._mixSamples(_f)

        xmin = None
        fmin = np.Inf

        if self.fast:
            xmin, fmin = super(MixSamplingOptimizer, self).optimize(f, df, f_df)
        else:
            # non fast mode bug in GpyOpt, so just move and fix the code here
            self.f = f
            self.df = df
            self.f_df = f_df

            def fp(x):
                '''
                Wrapper of *f*: takes an input x with size of the not fixed dimensions expands it and evaluates the entire function.
                '''
                x = np.atleast_2d(x)
                xx = self._expand_vector(x)
                if x.shape[0]==1:
                    return self.f(xx)[0]
                else:
                    return self.f(xx)

            def fp_dfp(x):
                '''
                Wrapper of the derivative of *f*: takes an input x with size of the not fixed dimensions expands it and evaluates the gradient of the entire function.
                '''
                x = np.atleast_2d(x)
                xx = self._expand_vector(x)

                fp_xx , dfp_xx = f_df(xx)
                dfp_xx = dfp_xx[:,np.array(self.free_dims)]
                return fp_xx, dfp_xx

            optByGradient = 0
            totalOpt = 0
            if self.f_df == None: fp_dfp = None # -- In case no gradients are available
            for i in range(self.samples.shape[0]):
                if self.search:
                    if fp_dfp is not None:
                        _, g1 = fp_dfp(self.samples[i])
                    if np.linalg.norm(g1) > 1e-17:
                        optByGradient += 1
                        x1, f1 = self.optimizer.optimize(self.samples[i], f =fp, df=None, f_df=fp_dfp)
                    else:
                        x1, f1 = self.samples[i], fp(self.samples[i])
                else:
                    x1, f1 = self.samples[i], fp(self.samples[i])

                if np.isnan(x1).any() or np.isnan(f1).any():
                    continue
                totalOpt += 1
                if f1<fmin:
                    xmin = x1
                    fmin = f1

        self.logger.info('[Total Sample, Opt Sample][%d, %d]' % (totalOpt, optByGradient))
        self.logger.info('[ACQ OPT RES][X]%s' % (xmin))
        self.logger.info('[ACQ OPT RES][ACQ]%f' % (fmin))
        self.logger.info('[ACQ OF BEST][ACQ_BEST]%s' % (_f(np.atleast_2d(self.best))))
        return xmin, fmin

class MSPOptimizer(ReferenceOptimizer):
    '''
    This class optimize the acq-function
    By sampling globaly and locally
    '''
    def __init__(self, space, optimizer='lbfgs', n_samples=50, fast=True, random=True, search=True, logger=None, **kwargs):
        super(MSPOptimizer, self).__init__(space, optimizer='lbfgs', n_samples=n_samples, fast=fast, random=random, search=search, **kwargs)
        self.premium_sample = n_samples
        self.dim = len(self.bounds)
        self.sample_s = 0.005 # variance when sampling locally

        self.logger = logger
        if not self.logger:
            self.logger = initLogger(self.__class__.__name__)

        self.n_samples = self.n_samples * self.dim * 100 # MSP sample number
        self.samples = np.empty(shape=(self.premium_sample, self.dim))

        self.lSampleNum = int(self.premium_sample / 5)
        self.gSampleNum = self.premium_sample - self.lSampleNum - 1

        self.opt_result = np.empty(shape=(self.premium_sample, 1))

        # support sqp opt method
        if optimizer == 'slsqp':
            self.optimizer = Opt_slsqp(space=self.space)
        self.logger.info('[PREM Samp][Glob Trail Samp][Opt Method]')
        self.logger.info('[%d][%d][%s]' % (self.premium_sample, self.n_samples, optimizer))

    def optimize(self, f=None, df=None, f_df=None):
        '''Prepare Samples, Then do optimization
           Half of the samples will be selected from global unif random
           The other half will be selected from best input sofar, use normal random
        '''
        optByGradient = 0
        assert(self.best is not None)

        # sampling around best result
        for k in range(self.dim):
            self.samples[:self.lSampleNum, k] = np.random.normal(
                loc=self.best[k],
                scale=self.sample_s,
                size=self.lSampleNum).clip(
                    min=self.bounds[k][0],
                    max=self.bounds[k][1])

        # add best result in sample
        self.samples[self.lSampleNum] = self.best

        # add best global samples
        globalSamples = np.empty(shape=(self.n_samples, self.dim))

        for k in range(self.dim):
            globalSamples[:,k] = np.random.uniform(
                low=self.bounds[k][0],
                high=self.bounds[k][1],
                size=self.n_samples)

        predictY = np.atleast_2d(f(globalSamples))
        self.samples[self.lSampleNum+1:, :] = globalSamples[predictY[:,0].argsort()[:self.gSampleNum], :]

        # only tries to optimize non-zero gredient samples
        def _fp(x):
            x = np.atleast_2d(x)
            return f(x)
        def _f_dfp(x):
            x = np.atleast_2d(x)
            return f_df(x)

        for i in range(self.premium_sample):
            self.opt_result[i], dydx = _f_dfp(self.samples[i])
            if np.linalg.norm(dydx) > 1e-17:
                optByGradient += 1
                self.samples[i,:], self.opt_result[i] = self.optimizer.optimize(self.samples[i], _fp, df, _f_dfp)

        # filter out nan
        validSamples = self.samples[np.apply_along_axis(lambda x: not(np.isnan(x).any()), 1, self.samples)]
        validResults = self.opt_result[np.apply_along_axis(lambda x: not(np.isnan(x).any()), 1, self.samples)]
        self.logger.info('[Total Sample, Opt Sample][%d, %d]' % (validSamples.shape[0], optByGradient))

        # return best sample
        xmin = validSamples[validResults[:,0].argmin()]
        fmin = f(np.atleast_2d(xmin))
        self.logger.info('[ACQ OPT RES][X]%s' % (xmin))
        self.logger.info('[ACQ OPT RES][ACQ]%s' % (str(fmin)))
        self.logger.info('[ACQ OF BEST][ACQ_BEST]%s' % (_fp(self.best)))
        return np.atleast_2d(xmin), fmin
