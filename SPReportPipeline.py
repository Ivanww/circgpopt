
import numpy as np

import SPReportHandler as SP
from Utils import initLogger

class SPReportPipeline(object):
    def __init__(self, config, logger=None):
        super(SPReportPipeline, self).__init__()
        self.config = config
        self.logger = logger
        if not self.logger:
            self.logger = initLogger(self.__class__.__name__)
        self.pipeline = SP.EmptyLineFilter(self.logger)
        self.handlers = []
        self.handlers.append(self.pipeline)
        self.monitor_number = 0
        self.ppconfig = None
        self.paras = {}
        self._buildPipeline()

    def _buildPipeline(self):
        self.monitor_num = self.config.monitor_num

        for m in self.config.monitors:
            if m[0] == 'filter':
                if m[1] == 'violation':
                    succ = SP.SPReportChecker(self.logger)
            elif m[0] == 'fether':
                succ = SP.SPParameterFetcher(m[1], self.logger)
            elif m[0] == 'constrain':
                succ = SP.ConstrainEvaluator(m[1], self.logger, m[2], m[3], float(m[4]))
            else:
                continue
            self.handlers[-1].successor(succ)
            self.handlers.append(succ)

    def resetPipeline(self):
        self.pipeline.reset()

    def summary(self):
        for handler in self.handlers:
            if isinstance(handler, SP.SPParameterFetcher):
                if handler.summary() is np.nan:
                    self.logger.warning('Extract value from monitor failed, set as inf')
                    self.logger.warning('%s = inf' % (handler.paraname))
                    self.paras[handler.paraname] = np.inf
                else:
                    self.paras[handler.paraname] = handler.summary()
        return self.paras

    def briefSummary(self):
        ret = np.array(list(map(lambda h: h.summary(), filter(lambda x: isinstance(x, SP.SPParameterFetcher), self.handlers))))
        if np.isnan(ret).any():
            self.logger.warning('Extract value from monitor failed, set as inf')
            ret[np.where(np.isnan(ret))] = np.inf
        return ret

    def process(self, line):
        self.pipeline.handle(line)

if __name__ == '__main__':
    import os
    import ConfigLoader as CL
    # config = CL.ConfigLoader('singleEndedConf.json')
    config = CL.ConfigLoader('twoStage.json')
    p = SPReportPipeline(config)

    config.logger.info('Workdir : %s' % (config.workdir))
    config.logger.info('nl filename : %s' % (config.nl_filename))

    spFilePath = os.path.join(config.workdir, config.nl_filename)
    config.logger.info(spFilePath)
    cmd = 'hspice ' + spFilePath
    os.chdir(config.workdir)
    report = os.popen(cmd)
    for line in report:
        p.process(line.rstrip())
    print(p.briefSummary())