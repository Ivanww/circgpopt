
import GPy
import numpy as np
import numpy.matlib as mt
from GPyOpt.models import BOModel

from Utils import initLogger
class CoregionalizedGPModel(BOModel):
    '''
    This class is the warpper of CoregionalizedGP
    Only implenment the basic API. Few options are supported for now
    '''
    analytical_gradient_prediction = True

    def __init__(self, output_num, kernel, noise_var=None, exact_feval=False, optimizer='bfgs', max_iters=1000, verbose=True):
            super(CoregionalizedGPModel, self).__init__()
            # TODO: add more options
            self.output_num = output_num
            self.kernel = kernel
            self.noise_var = noise_var
            self.exact_feval = exact_feval
            self.max_iters = max_iters
            self.verbose = verbose
            self.logger = initLogger(self.__class__.__name__)
            self.model = None

    def _createModel(self, xList, yList):
        assert(self.kernel is not None)
        self.model = GPy.models.GPCoregionalizedRegression(xList, yList, kernel=self.kernel)
        # TODO: more complicated config should be done here, hard code for now
        self.logger.info('[Config kernels]')
        self.model['.*ICM.*var'].unconstrain()
        self.model['.*ICM0.*var'].constrain_fixed(1.)
        self.model['.*ICM0.*W'].constrain_fixed(0)
        self.fmin = -np.inf
        self.min = None
        self.logger.info('[Start optimize model]')
        self.model.optimize()
        self.logger.info('[Optimize model done]')

    def expandX(self, x):
        '''
        Only predict one input for now.
        TODO: support multi input
        '''
        assert(np.atleast_2d(x).shape[0] == 1)
        index = np.arange(self.output_num).reshape((self.output_num, 1))
        Xs = mt.repmat(x, self.output_num, 1)
        return np.hstack([Xs, index])

    def updateModel(self, X_all, Y_all, X_new, Y_new):
        '''
        Always use a new model
        '''
        self._createModel(X_all, Y_all)
        # if self.model is None:
        #     self._createModel(X_all, Y_all)
        # else:
        #     self.model.set_XY(X_all, Y_all)

    def predict(self, X):
        dim = np.atleast_2d(X).shape[1]
        Xs = self.expandX(X) # predict all outputs
        noise_dict = {'output_index':Xs[:, dim:].astype(int)}
        m, v = self.model.predict(Xs, Y_metadata=noise_dict)

        return m, np.sqrt(v)

    def predict_withGradients(self, X):
        dim = np.atleast_2d(X).shape[1]
        Xs = self.expandX(X)
        noise_dict = {'output_index':Xs[:, dim:].astype(int)}
        m, v = self.model.predict(Xs, Y_metadata=noise_dict)
        dmdx, dvdx = self.model.predictive_gradients(Xs)
        dmdx = dmdx[:,:,0]
        dsdx = dvdx / (2*np.sqrt(v))

        return m, np.sqrt(v), dmdx, dsdx

    def get_fmin(self):
        '''
        The min value of a vector output is meaningless, hardcode for now
        '''
        return
