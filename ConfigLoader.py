import os
import json
import logging

from Utils import initLogger

class ConfigLoader(object):
    '''
    This class load and check config for optimization
    '''
    def __init__(self, configfile=None):
        object.__init__(self)
        self.userconfig = configfile
        self.data = dict()
        self._loadConfig('default.json')
        self.logger = initLogger('CircOpt')
        fileHandler = logging.FileHandler('..\opt.log')
        fileHandler.setFormatter(logging.Formatter('[%(asctime)s][%(levelname)s][%(name)s] %(message)s'))
        self.logger.addHandler(fileHandler)
        if not self.userconfig or not os.path.isfile(configfile):
            self.logger.critical("Cannot Load User Config!")
        else:
            self._loadConfig(self.userconfig)
        self.para_num = len(self.data['space'])
        self.para_name = [p['name'] for p in self.data['space']]
        self.monitor_num = len(list(filter(lambda x: x[0] == 'fether' or x[0] == 'constrain', self.data['monitors'])))
        self.constrainNum = self.monitor_num-1
        self.max_iter = self.data['max_iter']
        self.max_time = float(self.data['max_time'])
        self.displayConfig()

    def __getattr__(self, name):
        return self.data.get(name)

    def _checkConfig(self):
        # input parameters
        if self.data.get('para_num') == 0:
            self.logger.critical('At least one parameter is required')

        # monitors
        if self.data['monitor_num'] == 0:
            self.logger.critical('At least one monitor is required')

    def _loadConfig(self, configfile):
        with open(configfile, 'r') as f:
            data = json.load(f)
        for key in data.keys():
            self.data[key] = data[key]
        if not self.logger:
            return
        if self.data['log_level'] != 'info':
            if self.data['log_level'] == 'debug':
                self.logger.setLevel(logging.DEBUG)
            elif self.data['log_level'] == 'warning':
                self.logger.setLevel(logging.WARNING)

    def getConfig(self, key):
        return self.data.get(key)

    def displayConfig(self):
        self.logger.info('\n %s : %s' % ('Para Number', str(self.para_num)))
        self.logger.info('\n %s : %s' % ('Monitor Number', str(self.monitor_num)))
        self.logger.info('\n %s : %s' % ('Para Names : ', str(self.para_name)))
        for (key, val) in self.data.items():
            self.logger.info('%s : %s' % (key, val))

if __name__ == '__main__':
    logger = logging.getLogger('config loader testing')
    logger.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.INFO)
    consoleFormater = logging.Formatter('[%(asctime)s][%(levelname)s][%(name)s] %(message)s')
    consoleHandler.setFormatter(consoleFormater)
    logger.addHandler(consoleHandler)

    loader = ConfigLoader('singleEndedConf.json')
    loader.displayConfig()
    print(loader.monitors)