
from GPyOpt.util.general import get_quantiles
import numpy as np
from scipy import stats as st

from EIWithConstrainModels import EIWithConstrainModels

class ConstrainedLogWeightedEI(EIWithConstrainModels):
    '''
    This class compute weighted EI but will use log probility
    If tau is set, then use tau to calculate EI and weight it by log probility
    Or only calculate feasible probility
    '''
    def __init__(self, model, space, constrain_models, optimizer=None, cost_withGradients=None, jitter=0.01, logger=None):
        super(ConstrainedLogWeightedEI, self).__init__(model, space, constrain_models, optimizer=optimizer,cost_withGradients=cost_withGradients, jitter=jitter, logger=logger)

    def logProbOfFeasible(self, x):
        constrains = np.empty(shape=(self.constrNum, 2))
        for i in range(self.constrNum):
            m, s = self.constrainModels[i].predict(x)
            constrains[i][0] = m[0][0]
            constrains[i][1] = s[0][0]

        logQuant = st.norm.logcdf(0, constrains[:,0], constrains[:,1])
        logProbOfFeasible = logQuant.sum()

        return logProbOfFeasible

    def _compute_acq(self, x):
        f_acqu = self.logProbOfFeasible(x)

        if self.tau:
            EI = super(ConstrainedLogWeightedEI, self)._compute_acq(x)
            if abs(EI-0).any() < 1e-40:
                self.logger.warning('[EI is equal to 0(%s), adjust to 1e-40]' % (str(EI)))
                self.logger.warning('[Probility of Feasible][%s]' % (str(f_acqu)))
                EI[abs(EI-0)<1e-40] = 1e-40

            f_acqu = np.exp(f_acqu) * EI

            if abs(f_acqu).any() < 1e-100:
                self.logger.warning('[wEI may be too small]')
                self.logger.warning('[EI is %s]' % (str(EI)))
                self.logger.warning('[Probility of Feasible][%s]' % (str(f_acqu)))

        return f_acqu

    def _compute_acq_withGradients(self, x):
        dim = np.atleast_2d(x).shape[1]
        m = np.empty(shape=(self.constrNum,1))
        s = np.empty(shape=(self.constrNum,1))
        dmdx = np.empty(shape=(self.constrNum, dim))
        dsdx = np.empty(shape=(self.constrNum, dim))

        for i in range(self.constrNum):
            m[i], s[i], dmdx[i], dsdx[i] = self.constrainModels[i].predict_withGradients(x)

        dudx = (m * dsdx - dmdx * s) / s**2 # partial : constrNum * dim

        logProdPhi = st.norm.logcdf(0, m, s).sum()
        Phi = st.norm.cdf(0, m, s)
        phi = st.norm.pdf(0, m, s)

        # u = (0 - m) / s
        dlogPhidu = np.nan_to_num((phi / Phi).reshape((self.constrNum, 1))) # constrNum * 1
        dlogProdPhidx = np.apply_along_axis(np.sum, 0, dlogPhidu * dudx) # 1 * dim

        assert(dlogProdPhidx.shape == (dim,))
        if self.tau:
            EI, dEIdx = super(ConstrainedLogWeightedEI, self)._compute_acq_withGradients(x)
            if abs(EI-0).any() < 1e-40:
                self.logger.warning('[EI is equal to 0, adjust to 1e-40]')
                self.logger.info('[Log Probility of Feasible][%s]' % (str(logProdPhi)))
                EI[abs(EI-0)<1e-40] = 1e-40

            logwEI = np.log(EI) + logProdPhi
            dlogwEIdx = dlogProdPhidx + dEIdx / EI

            f_acqu = np.exp(logwEI)
            df_acqu = dlogwEIdx * f_acqu
        else:
            f_acqu = logProdPhi
            df_acqu = dlogProdPhidx
        # if np.isnan(f_acqu).any() or np.isnan(df_acqu).any():
        #     self.logger.warning('[INVALID ACQ]')
        return f_acqu, df_acqu

if __name__ == '__main__':
    pass
