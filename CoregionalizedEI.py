
import numpy as np
from scipy import stats as st
from GPyOpt.util.general import get_quantiles

from AcquisitionEIWithTau import AcquisitionEIWithTau
class CoregionalizedEI(AcquisitionEIWithTau):
    '''
    This class compute weithged EI, using log probility
    But it use a coregionalized GP model to handle target and constrains
    Only few options are supported now
    Note : DO NOT use normal _compute_acq, because it will call model.get_fmin(), which is of no meaning to vector output
    '''
    def __init__(self, model, space, output_num, optimizer=None, cost_withGradients=None, jitter=0.01, logger=None):
        super(CoregionalizedEI, self).__init__(model, space, optimizer, cost_withGradients=cost_withGradients, jitter=jitter, logger=logger)
        self.output_num = output_num

    def expandX(self, x, dim):
        ones = np.ones(shape=(x.shape[0], 1))
        ones.fill(dim)
        return np.hstack([x, ones])

    def _compute_acq(self, x):
        # predict, x : numX * dim
        numX = np.atleast_2d(x).shape[0]
        dim = np.atleast_2d(x).shape[1]
        # out * numX
        m = np.empty(shape=(self.output_num, numX))
        s = np.empty(shape=(self.output_num, numX))
        for i in range(numX):
            mm, ss = self.model.predict(x[i, :])
            m[:, i] = mm.flatten()
            s[:, i] = ss.flatten()
        # deal with constrains
        # constrNum * xnum
        logQuant = st.norm.logcdf(0, m[1:, :], s[1:, :])
        # 1 * xnum
        f_acqu = np.apply_along_axis(np.sum, 0, logQuant)
        # if feasible result exists
        if self.tau:
            # 1 * xnum
            phi, Phi, _ = get_quantiles(self.jitter, self.tau, m[0], s[0])
            # 1 * xnum
            EI = (self.tau - m + self.jitter) * Phi + s * phi
            # 1 * xnum
            f_acqu = np.exp(f_acqu) * EI
        return f_acqu

    def _compute_acq_withGradients(self, x):
        # predict
        # x : numX * dim
        dim = np.atleast_2d(x).shape[1]
        numX = np.atleast_2d(x).shape[0]
        # m, s : out * dim
        m = np.empty(shape=(self.output_num, numX))
        s = np.empty(shape=(self.output_num, numX))
        # dmdx, dsdx : out * numX * dim+1 (a dummy dim)
        dmdx = np.empty(shape=(self.output_num, numX, dim+1))
        dsdx = np.empty(shape=(self.output_num, numX, dim+1))
        for i in range(numX):
            mm, ss, dmdx[:, i, :], dsdx[:, i, :] = self.model.predict_withGradients(x[i, :])
            m[:, i] = mm.flatten()
            s[:, i] = ss.flatten()
        # remove dummy dim
        dmdx = dmdx[:, :, :-1]
        dsdx = dsdx[:, :, :-1]
        # out * numx * 1
        m = np.atleast_3d(m)
        s = np.atleast_3d(s)
        # partial : consNum * numX * dim
        dudx = (m[1:] * dsdx[1:] - dmdx[1:] * s[1:]) / s[1:]**2

        # numX * 1
        logProdPhi = np.apply_along_axis(np.sum, 0, st.norm.logcdf(0, m[1:], s[1:]))
        assert(logProdPhi.shape == (numX, 1))
        # consNum * numX * 1
        Phi = st.norm.cdf(0, m[1:], s[1:])
        phi = st.norm.pdf(0, m[1:], s[1:])
        # u = (0 - m) / s
        # constrNum * numX
        dlogPhidu = np.nan_to_num((phi / Phi).reshape((self.output_num-1, numX)))

        # numX * dim
        dlogProdPhidx = np.apply_along_axis(np.sum, 0, np.atleast_3d(dlogPhidu) * dudx)
        assert(dlogProdPhidx.shape == (numX, dim))
        # if feasible result exists
        if self.tau:
            # numX * 1
            phi, Phi, _ = get_quantiles(self.jitter, self.tau, m[0], s[0])
            # numX * 1
            EI = (self.tau - m[0] + self.jitter) * Phi + s[0] * phi
            # numX * dim
            dEIdx = dsdx[0] * phi - Phi * dmdx[0]
            logwEI = np.log(EI) + logProdPhi
            if (abs(EI-0) < 1e-40).any():
                self.logger.warning('[EI is equal to 0, adjust to 1e-40]')
                EI = 1e-40
            # numX * dim
            dlogwEIdx = dlogProdPhidx + dEIdx / EI
            # numX * 1
            f_acqu = np.exp(logwEI)
            # numX * dim
            df_acqu = dlogwEIdx * f_acqu
        else:
            f_acqu = logProdPhi
            df_acqu = dlogProdPhidx

        return f_acqu, df_acqu

if __name__ == '__main__':
    pass
