
import numpy as np
from GPyOpt.core.task.objective import Objective
from GPyOpt.util.stats import initial_design

from Utils import initLogger, regularSpace

class LoggerObjective(Objective):
    '''
    This class add an logger to Objective
    And act as a decorator
    '''
    def __init__(self, objective):
        super(LoggerObjective, self).__init__()
        self.objective = objective

        if getattr(objective, 'config', None):
            self.config = objective.config
        else:
            self.config = None

        if getattr(objective, 'logger', None):
            self.logger = objective.logger
        else:
            self.logger = None

        if not self.logger:
            self.logger = initLogger(self.__class__.__name__)

    def evaluate(self, X):
        raise NotImplementedError()

    def getCoreObjective(self):
        return self.objective.getCoreObjective()

    def __getattr__(self, name):
        return getattr(self.objective, name, None)

class NormalizedObjective(LoggerObjective):
    '''
    This class decorator an concrete or decorator to expand normalized input
    Note if a decorator wraps this class, its space must be limited to [-1, 1]
    '''
    def __init__(self, space, objective):
        super(NormalizedObjective, self).__init__(objective=objective)
        self.bounds = np.array(space.get_bounds())
        self.k = (self.bounds[:,1] - self.bounds[:,0]) / 2
        self.b = (self.bounds[:,1] + self.bounds[:,0]) / 2

    def evaluate(self, X):
        realX = self.k * X + self.b
        self.logger.info('[formal X]%s' % (str(realX)))
        return self.objective.evaluate(realX)

class InvalidResultFilter(LoggerObjective):
    '''
    This class will handle the failure of it decoratee
    If none of the evaluation successes, it will generate a new batch of input
    until at least one evaluation is successful
    And then store the valid input in validInput
    '''
    def __init__(self, space, objective, validate_func=None):
        super(InvalidResultFilter, self).__init__(objective=objective)
        self.validInput = None
        self.isAllValid = True
        self.space = space
        if validate_func is not None:
            self.validate = lambda x: np.apply_along_axis(validate_func, 1, x)
        else:
            def notNanOrInf(x):
                return not(np.isnan(x).any()) and not(np.isinf(x).any())
            self.validate = lambda x: np.apply_along_axis(notNanOrInf, 1, x)

    def evaluate(self, X):
        self.validInput = X
        self.isAllValid = True

        fx, cost = self.objective.evaluate(self.validInput)

        fx = fx[self.validate(fx)]

        if np.atleast_2d(fx).shape[0] == 0:
            self.isAllValid = False
            n = X.shape[0]
            while np.atleast_2d(fx).shape[0] == 0:
                self.logger.info('[All inputs are invalid, use random samples instead]')
                self.validInput = initial_design('random', self.space, n)
                fx, cost = self.objective.evaluate(self.validInput)
                fx = fx[self.validate(fx)]

        if fx.shape[0] != X.shape[0]:
            self.logger.info('[Some inputs are invalid, removed!]')
            self.isAllValid = False
            self.validInput = self.validInput[self.validate(fx)]

        cost = np.asarray(cost)
        cost = cost[self.validate(fx)]
        return fx, cost.tolist()

if __name__ == '__main__':
    from ConfigLoader import ConfigLoader
    c = ConfigLoader('singleEndedConf.json')
    import GPyOpt
    from Utils import regularSpace
    orig_space = GPyOpt.Design_space(space=c.space)
    regular_space = GPyOpt.Design_space(regularSpace(c.space))
    from SPObjective import SpiceObjective
    obj = NormalizedObjective(orig_space, SpiceObjective(c, orig_space))
    x = np.random.uniform(-1, 1, c.para_num)
    print(x)
    if obj is not None:
        print(obj.evaluate(np.atleast_2d(x)))
        # f_eval, cost = obj.evaluate(np.atleast_2d(x))
        # print(f_eval)
        # print(cost)
