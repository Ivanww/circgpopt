
import GPyOpt
import GPy
import numpy as np

from ConfigLoader import ConfigLoader
from CoregionalizedGPModel import CoregionalizedGPModel
from CoregionalizedEI import CoregionalizedEI
from MSPOptimizer import MixSamplingOptimizer
from SPObjective import SpiceObjective
from ObjectiveDecorator import NormalizedObjective, InvalidResultFilter
from CoregionalizeBayesianOpt import CoregionalizedBayesianOpt
from Utils import regularSpace

def coregionalizedOpt():
    # load config
    config = ConfigLoader('singleEndedConf.json')

    # regular input to [-1, 1]
    space = GPyOpt.Design_space(space = config.space)
    regularS = GPyOpt.Design_space(space=regularSpace(config.space))

    # kernels
    dim = 24 # hard code for now
    output_num = 7 # hard code for now
    kern0 = GPy.kern.Bias(input_dim=dim)
    kern1 = GPy.kern.RBF(input_dim=dim, ARD=True)
    K = GPy.util.multioutput.LCM(input_dim=dim, num_outputs=output_num, kernels_list=[kern0, kern1])

    # model
    model = CoregionalizedGPModel(output_num, K)

    # Acquisition Function Optimizer
    acqOptimizer = MixSamplingOptimizer(regularS, optimizer='slsqp', n_samples=60, logger=config.logger)

    # initial X
    initialDesign = GPyOpt.util.stats.initial_design(config.init_sample_method, regularS, config.init_sample_size)

    # Acquistion Function
    acquisition = CoregionalizedEI(model, space, output_num, optimizer=acqOptimizer, logger=config.logger)

    # Wrapper for Acquisition Function
    evaluator = GPyOpt.core.evaluators.Sequential(acquisition)

    # SPice Objective : Problem
    objective = InvalidResultFilter(regularS, NormalizedObjective(space, SpiceObjective(config)))

    # Bayesian Optimization Manager
    bo = CoregionalizedBayesianOpt(model, space, objective, acquisition, evaluator, initialDesign, logger=config.logger)

    # Run Optimization
    # try:
    bo.run_optimization(
        max_iter=1000,
        max_time=config.max_time,
        eps=config.opt_eps,
        verbosity = True,
        save_models_parameters=config.save_models_parameters,
        report_file=config.report_file,
        evaluations_file=config.evaluations_file,
        models_file=config.models_file
    )
    # except:
    #     print('Dumping data...')
    #     np.savetxt('core_dump_X.txt', bo.X, delimiter=',')
    #     np.savetxt('core_dump_Y.txt', bo.Y, delimiter=',')
    #     print('Dump data done, exiting...')
    #     exit(1)

    # Report Result
    print(bo.fx_opt)
    print(bo.x_opt)

if __name__ == '__main__':
    coregionalizedOpt()