
import numpy as np
import GPyOpt
from GPyOpt.core.task.objective import Objective

from InvalidSafeObjective import InvalidSafeObjective
from MSPOptimizer import MSPOptimizer, MixSamplingOptimizer

def constrain1(X):
    assert(X.ndim == 2)
    assert(X.shape[1] == 2)
    return (-X[:,1] - 0.5 + np.abs(X[:,0]) - np.sqrt(1-X[:,1]**2)).reshape((X.shape[0], 1))

def constrain2(X):
    assert(X.ndim == 2)
    assert(X.shape[1] == 2)
    return (X[:,1] + 0.5 + np.abs(X[:,0]) - np.sqrt(1-X[:,1]**2)).reshape((X.shape[0],1))

def sixhumpcamelC(x):
    x = np.atleast_2d(x)
    func = GPyOpt.objective_examples.experiments2d.sixhumpcamel()
    return func.f(x), constrain1(x), constrain2(x)

def objFunc(X):
    return np.apply_along_axis(sixhumpcamelC, 1, X)

class ToyObjective(Objective):
    def __init__(self):
        super(ToyObjective, self).__init__()

    def evaluate(self, X):
        return objFunc(X), np.ones(shape=(X.shape[0],))

def evalInitY(objective, X_init):
    '''
    This function calculate init_X
    And update init_X if necessary (some evaluations fail)
    '''
    Y_init, costValues = objective.evaluate(X_init)

    if getattr(objective, 'isAllValid', None) is not None:
        if not objective.isAllValid:
            return objective.validInput, Y_init

    # for objective dose not has isAllValid attr
    return X_init, Y_init

def testConstrainedLogEI():
    space = [
        {'name':'x1', 'type':'continuous','domain':(-1,1)},
        {'name':'x2', 'type':'continuous','domain':(-1.5,1.5)}
    ]
    region = GPyOpt.Design_space(space=space)

    initial_design = GPyOpt.util.stats.initial_design('random', region, 10)

    from ObjectiveDecorator import InvalidResultFilter
    toyObj = InvalidResultFilter(region, ToyObjective())
    X_init, Y_init = evalInitY(toyObj, initial_design)
    import GPy
    mkernel = GPy.kern.RBF(2, 1., None, True)
    mmodel = GPyOpt.models.GPModel(
        kernel=mkernel,
        noise_var=None,
        exact_feval=False,
        normalize_Y=False,
        optimizer='bfgs',
        max_iters=50,
        optimize_restarts=5,
        sparse=False,
        num_inducing=10,
        verbose=False
    )

    # acqOpt = MSPOptimizer(region, optimizer='slsqp')
    # acqOpt = MSPOptimizer(region, optimizer='lbfgs')
    # acqOpt = GPyOpt.optimization.AcquisitionOptimizer(region)
    acqOpt = MixSamplingOptimizer(region, 'slsqp', 50)
    from ConstrainedLogWeightedEI import ConstrainedLogWeightedEI
    mconstrainModels = []
    for i in range(2):
        mconstrainModels.append(
            GPyOpt.models.GPModel(
                kernel=GPy.kern.RBF(2, ARD=True),
                # kernel=None,
                noise_var=None,
                exact_feval=False,
                normalize_Y=False,
                optimizer='bfgs',
                max_iters=1000,
                optimize_restarts=5,
                sparse=False,
                num_inducing=10,
                verbose=False
            )
        )
    acquisition = ConstrainedLogWeightedEI(
        model=mmodel,
        space=region,
        constrain_models=mconstrainModels,
        optimizer=acqOpt,
        cost_withGradients=None,
        jitter=1e-10,
        logger=None
    )

    evaluator = GPyOpt.core.evaluators.Sequential(acquisition)
    from EpsilonGreedyByOpt import EpsilonGreedyBayesianOptimization
    bo = EpsilonGreedyBayesianOptimization(
        model=mmodel,
        space=region,
        objective=toyObj,
        acquisition=acquisition,
        evaluator=evaluator,
        X_init=initial_design,
        Y_init=None,
        cost=None,
        normalize_Y=None,
        model_update_interval=1,
        # epsilon=0.05,
        # logger=None
    )

    bo.run_optimization(max_iter=10)

    print(bo.x_opt)
    print(bo.fx_opt)
    print(bo.X)
    print('='*20)
    print(bo.Y)
    bo.plot_acquisition()
    bo.plot_convergence()

if __name__ == '__main__':
    testConstrainedLogEI()
