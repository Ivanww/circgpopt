import time

import numpy as np
import GPy
import GPyOpt
from GPyOpt.core.bo import BO

from AcquisitionEIWithTau import AcquisitionEIWithTau
from EIWithConstrainModels import EIWithConstrainModels
from ConstrainedWeightedEI import ConstrainedWeightedEI
from MSPOptimizer import MSPOptimizer, ReferenceOptimizer
from Utils import initLogger, feasible, bestValues, bestXY, dist, isBetter

class EpsilonGreedyBayesianOptimization(BO):
    '''
    This class will use a tiny threshold, epsilon, to decermine whether to use a random sample in next round of opt
    Add some check in standard process to work compatible with self-defined acq and opt
    '''
    def __init__(self, model, space, objective, acquisition, evaluator, X_init, Y_init=None, cost=None, normalize_Y=None, model_update_interval=1, epsilon=0.05, logger=None):
        # options
        self.initial_iter = True
        self.modular_optimization = True
        self.epsilon = epsilon
        self.notBetter = model_update_interval
        self.bounds = space.get_bounds()
        # logger
        self.logger = logger
        if not self.logger:
            self.logger = initLogger(self.__class__.__name__)

        # initializing Y
        self.logger.info('\n%s\n' % ('='*50))
        if Y_init is None:
            self.logger.info('evaluate init_Y...')
            X_init, Y_init = self._evalInitY(objective, X_init)
        self.logger.info('[Initial Data Size][X: %s][Y: %s]' % (X_init.shape, Y_init.shape))
        np.savetxt('initX.txt', X_init, delimiter=',')
        np.savetxt('initY.txt', Y_init, delimiter=',')
        # init options
        super(EpsilonGreedyBayesianOptimization, self).__init__(
            model=model, space=space, objective=objective, acquisition=acquisition,
            evaluator=evaluator, X_init=X_init, Y_init=Y_init, cost=cost,
            normalize_Y=normalize_Y, model_update_interval=model_update_interval)

        # find best in init samples
        self.curBestX, self.curBestY = bestXY(self.X, self.Y)
        if isinstance(self.acquisition.optimizer, ReferenceOptimizer):
            self.acquisition.optimizer.updateBest(self.curBestX)

        self.logger.info('\n%s\n' % ('='*50))

    def _evalInitY(self, objective, X_init):
        '''
        This function calculate init_X
        And update init_X if necessary (some evaluations fail)
        '''
        self.logger.info('initializing optimization...')
        Y_init, costValues = objective.evaluate(X_init)

        if getattr(objective, 'isAllValid', None) is not None:
            if not objective.isAllValid:
                self.logger.info('[eliniminate invalid X...]')
                return objective.validInput, Y_init

        # for objective dose not has isAllValid attr
        return X_init, Y_init

    def _postCheckSample(self):
        '''
        if too close to known x, use a random trial, only for SINGLE sample
        '''
        assert(self.suggested_sample.shape[0] == 1)
        for i in range(self.X.shape[0]):
            d = dist(self.suggested_sample, self.X[i,:])
            if d < 1e-17:
                self.logger.info('[Avoid duplicate evaluation]')
                bounds = self.space.get_bounds()
                for k in range(self.X.shape[1]):
                    self.suggested_sample[0][k] = np.random.normal(
                        loc=self.suggested_sample[0][k],
                        scale=1e-3,
                        size=1).clip(
                            min=bounds[k][0],
                            max=bounds[k][1])
                break

    def _update_model(self):
        '''
        update the main model first
        if there are models for constrains, update them then
        '''
        self.logger.info('[updating models...][Data Size][%s][%s]' % (str(self.X.shape), str(self.Y.shape)))

        if self.Y.shape[1] == 1:
            # TODO: handle exception here!!
            self.logger.info('1-D output, only update main model')
            if self.num_acquisitions % self.model_update_interval == 0:
                super(EpsilonGreedyBayesianOptimization, self)._update_model()
        else:
            if self.notBetter == self.model_update_interval:
                self.notBetter = 0
                self.logger.info('[updateing main model....]')
                while True:
                    try:
                        if self.normalize_Y and self.Y.shape[0] > 1:
                            self.model.updateModel(self.X, ((self.Y[:,0]-self.Y[:,0].mean())/(self.Y[:,0].std())).reshape((self.Y.shape[0], 1)), None, None)
                        else:
                            self.model.updateModel(self.X, self.Y[:,0].reshape(self.Y.shape[0], 1), None, None)
                    except np.linalg.linalg.LinAlgError:
                        # if building model fails, try to rebuild it
                        self.logger.warning('[Updating main model fail, rebuilding...]')
                        self.model = GPyOpt.models.GPModel(
                            kernel = GPy.kern.RBF(self.X.shape[0], ARD=True),
                            noise_var=None,
                            exact_feval=False,
                            normalize_Y=False, # True
                            optimizer='bfgs',
                            max_iters=1000,
                            optimize_restarts=1,
                            sparse=False,
                            num_inducing=10,
                            verbose=False # False
                        )
                        continue
                    break

                if isinstance(self.acquisition, EIWithConstrainModels):
                    self.logger.info('[updating models for constrains....]')
                    # update constrain
                    self.acquisition.updateConstrainModel(self.X, self.Y, self.suggested_sample, self.Y_new)
                    self.acquisition.updateTau(self.Y)
                # self._reportModels()
        self.logger.info('[update models done!]')

    def _predict_result(self):
        '''
        predict the result according to current model(s)
        '''
        m, s= self.model.predict(self.suggested_sample)
        self.logger.info('[Mean Pred]%s' % (str(m)))
        self.logger.info('[S Pred]%s' % (str(s)))
        if isinstance(self.acquisition, EIWithConstrainModels):
            means = np.empty(shape=(1, self.acquisition.constrNum))
            ss = np.empty(shape=(1, self.acquisition.constrNum))
            for i in range(self.acquisition.constrNum):
                means[0][i], ss[0][i] = self.acquisition.constrainModels[i].predict(self.suggested_sample)
            self.logger.info('[Constrains Pred]%s' % (str(means)))
            self.logger.info('[Constrain S Pred]%s' % (str(ss)))

    def _compute_results(self):
        self.fx_opt = min(self.Y[feasible(self.Y), 0])
        self.x_opt = self.X[self.Y[:,0] == self.fx_opt]
        self.Y_best = bestValues(self.Y)

    def _compute_next_evaluations(self):
        self.curBestX, self.curBestY = bestXY(self.X, self.Y)
        self.logger.info('[Current Best X] %s' % (str(self.curBestX)))
        self.logger.info('[Current Best Y] %s' % (str(self.curBestY)))
        if isinstance(self.acquisition.optimizer, ReferenceOptimizer):
            self.acquisition.optimizer.updateBest(self.curBestX)
        return super(EpsilonGreedyBayesianOptimization, self)._compute_next_evaluations()

    def evaluate_objective(self):
        self.Y_new, cost_new = self.objective.evaluate(self.suggested_sample)

        # only select volid input
        if getattr(self.objective, 'isAllValid', None) is not None:
            if not self.objective.isAllValid:
                self.suggested_sample = self.objective.validInput
                self.logger.info('[Add Sample] %s' % (str(self.suggested_sample)))

        self.logger.info('[Dist to BestX] [%f]' % (dist(self.suggested_sample, self.curBestX)))
        self.logger.info('[New Y] %s' % (str(self.Y_new)))

        self.cost.update_cost_model(self.suggested_sample, cost_new)
        self.Y = np.vstack((self.Y,self.Y_new))

    def _reportModels(self):
        # main model summary
        mainModelInfo = str(self.model.model)
        self.logger.info('[Main Model Report]\n %s' % (mainModelInfo))

        # main model kernel
        kern = self.model.model
        if getattr(kern, 'sum', None):
            kern = kern.sum
        if getattr(kern, 'rbf', None):
            self.logger.info('[RBF Kernel Info]\n %s' % (str(kern.rbf)))
            self.logger.info('[RBF Kernel Info]\n %s' % (str(kern.rbf.lengthscale)))

        # report constrain models, if has
        if isinstance(self.acquisition, EIWithConstrainModels):
            for i in range(len(self.acquisition.constrainModels)):
                m = self.acquisition.constrainModels[i].model
                self.logger.info('[Constrain Model %d]\n%s' % (i, str(m)))
                if getattr(m, 'sum', None):
                    m = m.sum

                if getattr(m, 'rbf', None):
                    self.logger.info('[RBF Kernel Info]\n%s' % (str(m.rbf)))
                    self.logger.info('[RBF Kernel Info]\n%s' % (str(m.rbf.lengthscale)))

    def run_optimization(self, max_iter = 0, max_time = np.inf,  eps = 1e-8, verbosity=True, save_models_parameters= True, report_file = None, evaluations_file= None, models_file=None):
        # update options
        self.verbosity = verbosity
        self.save_models_parameters = save_models_parameters
        self.report_file = report_file
        self.evaluations_file = evaluations_file
        self.models_file = models_file
        self.model_parameters_iterations = None
        # --- Check if we can save the model parameters in each iteration
        if self.save_models_parameters == True:
            if not (isinstance(self.model, GPyOpt.models.GPModel) or isinstance(self.model, GPyOpt.models.GPModel_MCMC)):
                print('Models printout after each iteration is only available for GP and GP_MCMC models')
                self.save_models_parameters = False
        # --- Setting up stop conditions
        self.eps = eps
        if  (max_iter == None) and (max_time == None):
            self.max_iter = 0
            self.max_time = np.inf
        elif (max_iter == None) and (max_time != None):
            self.max_iter = np.inf
            self.max_time = max_time
        elif (max_iter != None) and (max_time == None):
            self.max_iter = max_iter
            self.max_time = np.inf
        else:
            self.max_iter = max_iter
            self.max_time = max_time

        # --- Initialize iterations and running time
        self.time_zero = time.time()
        self.cum_time  = 0
        self.num_acquisitions = 0

        self.suggested_sample = self.X
        self.Y_new = self.Y

        self.logger.info('bayesian process start!')
        while self.max_time > self.cum_time:
            self.logger.info('\n%s\n' % ('='*50))
            self.logger.info('Current iteration : %d' % (self.num_acquisitions))

            self._update_model()
            # try:
            #     self._update_model()
            # except np.linalg.linalg.LinAlgError:
            #     self.logger.error('update model failed, trying to fix it')

            # next sample
            if np.random.ranf() < self.epsilon:
                self.logger.info('[use a random sample]')
                self.suggested_sample = GPyOpt.util.stats.initial_design('random', self.space, 1)
            else:
                self.suggested_sample = self._compute_next_evaluations()
            self._postCheckSample()
            self.logger.info('[Suggested Sample] %s' % (str(self.suggested_sample)))

            # predict on suggestion
            self._predict_result()

            # evaluate suggestion
            self.evaluate_objective()

            # add into database
            self.X = np.vstack((self.X, self.suggested_sample))

            # update options
            if isBetter(self.curBestY, self.Y_new):
                self.notBetter = 0

                if isinstance(self.acquisition, AcquisitionEIWithTau):
                    self.acquisition.updateTau(self.Y_new)

                if isinstance(self.acquisition.optimizer, ReferenceOptimizer):
                    self.acquisition.optimizer.updateBest(self.suggested_sample)
            else:
                self.notBetter += 1
            self.logger.info('[Not Better] %d' % (self.notBetter))

            self.cum_time = time.time() - self.time_zero
            self.num_acquisitions += 1
            if (self.num_acquisitions > self.max_iter):
                break

        self._compute_results()

        if self.report_file != None:
            self.save_report(self.report_file)
        if self.evaluations_file != None:
            self.save_evaluations(self.evaluations_file)
        if self.models_file != None:
            self.save_models(self.models_file)

if __name__ == '__main__':
    import GPy
    func  = GPyOpt.objective_examples.experiments2d.branin()
    func.plot()
    objective = GPyOpt.core.task.SingleObjective(func.f)
    space = GPyOpt.Design_space(space =[{'name': 'var_1', 'type': 'continuous', 'domain': (-5,10)},
                                        {'name': 'var_2', 'type': 'continuous', 'domain': (1,15)}])
    model = GPyOpt.models.GPModel(kernel=GPy.kern.RBF(2,ARD=True),
        optimize_restarts=2,verbose=False)
    aquisition_optimizer = GPyOpt.optimization.AcquisitionOptimizer(space)
    initial_design = GPyOpt.util.stats.initial_design('random', space, 10)
    acquisition = GPyOpt.acquisitions.AcquisitionEI(model, space, optimizer=aquisition_optimizer)
    evaluator = GPyOpt.core.evaluators.Sequential(acquisition)

    bo = EpsilonGreedyBayesianOptimization(model, space, objective, acquisition, evaluator, initial_design)

    max_iter  = 30
    print('starting optimization')
    bo.run_optimization(max_iter = max_iter)
    bo.plot_acquisition()
    bo.plot_convergence()