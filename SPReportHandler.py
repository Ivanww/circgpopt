import re

from numpy import nan

class SPReportHandler(object):
    '''Handler for report'''
    def __init__(self, logger, successor=None):
        object.__init__(self)
        self.succ = successor
        self.logger = logger

    def __str__(self):
        return ''

    def _handle(self, line):
        return False

    def _reset(self):
        pass

    def successor(self, succ):
        self.succ = succ

    def handle(self, line):
        if self._handle(line):
            pass
        else:
            if self.succ:
                self.succ.handle(line)

    def reset(self):
        self._reset()
        if self.succ:
            self.succ.reset()

    def summary(self):
        return None


class SPReportChecker(SPReportHandler):
    '''concrete handler to check the report validation'''
    def __init__(self, logger, successor=None):
        SPReportHandler.__init__(self, logger, successor)
        self.isFail = re.compile(r'hspice job aborted')
        self.valid = True

    def _handle(self, line):
        if not self.valid:
            return True

        if self.isFail.search(line):
            self.logger.error('Simulation Failed!')
            self.valid = False
            return True
        else:
            return False
    def _reset(self):
        self.valid = True

    def summary(self):
        return self.valid

class SPParameterFetcher(SPReportHandler):
    '''check and fetch one parameter'''
    def __init__(self, paraname, logger, default=nan, successor=None):
        SPReportHandler.__init__(self, logger, successor)
        self.paraname = paraname
        self.fetcher = re.compile(r'\b' + paraname + r'=\s*(-?\d*\.?\d+[E|e][+-]\d+)')
        self.default = default
        self.paraValue = self.default

    def __str__(self):
        return self.paraname + ' ' + self.paraValue

    def _handle(self, line):
        match = self.fetcher.search(line)
        if match:
            self.paraValue = float(match.group(1))
            return True
        else:
            return False

    def _reset(self):
        self.paraValue = self.default

    def summary(self):
        return self.paraValue

class ConstrainEvaluator(SPParameterFetcher):
    def __init__(self, paraname, logger, constrain, less=True, default=nan, successor=None):
        super(ConstrainEvaluator, self).__init__(paraname, logger, default, successor)
        self.constrain = constrain
        self.less = less
        self._reset()

    def _handle(self, line):
        if super(ConstrainEvaluator, self)._handle(line):
            if self.less:
                self.allowance = self.paraValue - self.constrain
            else:
                self.allowance = self.constrain - self.paraValue
            return True
        else:
            return False

    def summary(self):
        return self.allowance

    def _reset(self):
        super(ConstrainEvaluator, self)._reset()
        if self.less:
            self.allowance = self.default - self.constrain
        else:
            self.allowance = self.constrain - self.default

class EmptyLineFilter(SPReportHandler):
    '''concrete handler to filter out empty line'''
    def __init__(self, logger, successor=None):
        SPReportHandler.__init__(self, logger, successor)
        self.isEmpty = re.compile(r'^\s*$')

    def _handle(self, line):
        if self.isEmpty.match(line):
            return True
        else:
            return False

if __name__ == '__main__':
    pass
