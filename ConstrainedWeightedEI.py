
import numpy as np
from GPyOpt.util.general import get_quantiles

from EIWithConstrainModels import EIWithConstrainModels

# TODO : This class is deprecated!!
class ConstrainedWeightedEI(EIWithConstrainModels):
    '''
    This class calculate the EI if constrains is feasible
    Or calculate the probility to make output feasible
    All models (model for output and models for constrains) share the same data

    model should be ByOpt GPModel
    '''
    def __init__(self, model, space, constrNum, optimizer=None, cost_withGradients=None, jitter=0.01, logger=None):
        super(ConstrainedWeightedEI, self).__init__(model, space, constrNum, optimizer,
            cost_withGradients=cost_withGradients, jitter=jitter, logger=logger)

    def _compute_acq(self, x):
        constrains = np.array([model.predict(x) for model in self.constrainModels])
        # if s is less than 1e-10, will be considered as 1e-10
        quantiles = np.array([get_quantiles(self.jitter, 0, x[0], x[1]) for x in constrains])
        f_acqu = quantiles[:,1].prod()
        if self.tau:
            f_acqu *= super(ConstrainedWeightedEI, self)._compute_acq(x)

        return f_acqu

    def _compute_acq_withGradients(self, x):
        constrains = np.empty([0, 4])
        for model in self.constrainModels:
            m, s, dmdx, dsdx = model.predict_withGradients(x)
            dmdx = dmdx.sum()
            dsdx = dsdx.sum()
            constrains = np.vstack((constrains, np.array([m, s, dmdx, dsdx])))
        # constrains = np.array([model.predict_withGradients(x) for model in self.constrainModels])
        m = constrains[:, 0]
        s = constrains[:, 1]
        dmdx = constrains[:, 2]
        dsdx = constrains[:, 3]

        quantiles = np.array([get_quantiles(self.jitter, 0, x[0], x[1]) for x in constrains])
        prodPhi = quantiles[:,1].prod()
        dprodPhidx = ((prodPhi / quantiles[:,1]) * quantiles[:,0]).sum()
        dPhidu = quantiles[:,0]
        dudx = (m * dsdx - dmdx * s) / s**2
        # if feasible output exists, calcuate EI
        if self.tau:
            EI, dEIdx = super(ConstrainedWeightedEI, self)._compute_acq_withGradients(x)
            f_acqu = EI * prodPhi
            df_acqu = dEIdx * prodPhi + EI * dprodPhidx
            df_acqu *= (dPhidu * dudx).prod()
        else:
            f_acqu = prodPhi
            df_acqu = dprodPhidx

        return f_acqu, df_acqu
