
import numpy as np
from scipy import optimize
from GPyOpt.optimization.optimizer import Optimizer

class Opt_slsqp(Optimizer):
    def __init__(self, space, maxiter=1000):
        super(Opt_slsqp, self).__init__(space)
        self.maxiter = maxiter

    def optimize(self, x0, f=None, df=None, f_df=None):
        # wrapper a function then optimize it
        if df is None and f_df is not None:
            def _df(x):
                return f_df(x)[1]

        if df is not None:
            out = optimize.minimize(f, x0, method='SLSQP', jac=df, bounds=self.space.get_bounds(), options={'disp':False})
            # out = optimize.minimize(f, x0, method='SLSQP', jac=df, options={'disp':True})
        else:
            out = optimize.minimize(f, x0, method='SLSQP', jac=_df, bounds=self.space.get_bounds(), options={'disp':False})
            # out = optimize.minimize(f, x0, method='SLSQP', jac=_df, options={'disp':True})
        return np.atleast_2d(out.x), f(out.x)

if __name__ == '__main__':
    def f(x):
        return -1*(2*x[0]*x[1] + 2*x[0] - x[0]**2 - 2*x[1]**2)

    def df(x):
        dfdx0 = -1*(-2*x[0] + 2*x[1] + 2)
        dfdx1 = -1*(2*x[0] - 4*x[1])
        return np.array([dfdx0, dfdx1])

    def f_df(x):
        fx = -1*(2*x[0]*x[1] + 2*x[0] - x[0]**2 - 2*x[1]**2)
        dfdx0 = -1*(-2*x[0] + 2*x[1] + 2)
        dfdx1 = -1*(2*x[0] - 4*x[1])
        return fx, np.array([dfdx0, dfdx1])

    import GPyOpt
    space = GPyOpt.Design_space(space =[{'name': 'x1', 'type': 'continuous', 'domain': (-1,1)},
                                        {'name': 'x2', 'type': 'continuous', 'domain': (-1,1)}])
    print(space.get_bounds())
    op = Opt_slsqp(space)
    res = op.optimize((-1, 1), f=f, df=None, f_df=f_df)
    print(res)