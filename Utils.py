import logging
import copy

import numpy as np

def reportModel(model):
    # main model summary
    mainModelInfo = str(model.model)
    print('[Main Model Report]\n %s' % (mainModelInfo))

    # main model kernel
    kern = model.model
    if getattr(kern, 'sum', None):
        kern = kern.sum
    if getattr(kern, 'rbf', None):
        print('[RBF Kernel Info]\n %s' % (str(kern.rbf)))
        print('[RBF Kernel Info]\n %s' % (str(kern.rbf.lengthscale)))



def initLogger(name):
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.DEBUG)
    consoleFormater = logging.Formatter('[%(asctime)s][%(levelname)s][%(name)s] %(message)s')
    consoleHandler.setFormatter(consoleFormater)
    logger.addHandler(consoleHandler)
    return logger

def feasible(X):
    return np.apply_along_axis(lambda l:(l[1:]<=0).all(), 1, X)

def feasibleValues(X):
    return X[feasible(X)]

def bestXY(X, Y, sign=1):
    '''According to Y, find best input X'''
    feasY = Y[feasible(Y)]
    if feasY.size > 0:
        feasX = X[feasible(Y)]
        if sign == 1:
            return feasX[feasY[:,0].argmin()], feasY[feasY[:,0].argmin()]
        else:
            return feasX[feasY[:,0].argmax()], feasY[feasY[:,0].argmax()]
    else:
        vioSum = np.apply_along_axis(lambda x : np.sum(x[x>0]), 1, Y[:, 1:])
        return X[vioSum.argmin()], Y[vioSum.argmin()]

def isBetter(oldY, newY):
    oldY = np.atleast_2d(oldY)
    newY = np.atleast_2d(newY)
    assert(oldY.shape[0] == 1)
    assert(newY.shape[0] == 1)
    if feasible(newY).all():
        if feasible(oldY).all():
            return newY[0][0] < oldY[0][0]
        else:
            return True
    else:
        if feasible(oldY).all():
            return False
        else:
            vio1 = newY[0][1:]
            vio2 = oldY[0][1:]
            return vio1[vio1>0].sum() < vio2[vio2>0].sum()


def bestValues(X, sign=1):
    n = X.shape[0]
    X_best = np.ones(n)
    for i in range(n):
        feasX = feasibleValues(X[:i+1, :])
        if feasX.size > 0:
            if sign == 1:
                X_best[i] = feasX[feasX[:,0].argsort()[0]][0]
            else:
                X_best[i] = feasX[feasX[:,0].argsort()[-1]][0]
        else:
            if sign == 1:
                X_best[i] = X[X[:i+1,0].argsort()[0]][0]
            else:
                X_best[i] = X[X[:i+1,0].argsort()[-1]][0]

    return X_best

def dist(X, Y):
    return np.sqrt(((X-Y)**2).sum())

def validate(X):
    return np.where(
        np.apply_along_axis(
            lambda x: not(np.isnan(x).any() or np.isinf(x).any()),
            1,
            X
        )
    )

def validate1(x):
    return not(np.isnan(x).any() or np.isinf(x).any())

def regularSpace(space):
    '''regular a space to [-1,1]
       Input : a list of dict, each dict describes an input
    '''
    regular = copy.deepcopy(space)
    for para in regular:
        para['domain'] = [-1, 1]

    return regular
