
import GPy
from GPyOpt.models import GPModel

class GPModelWithMean(GPModel):
    analytical_gradient_prediction = True

    def __init__(self, kernel=None, mean_function=None, noise_var=None, exact_feval=False, normalize_Y=True, optimizer='bfgs', max_iters=1000, optimize_restarts=5, sparse=False, num_inducing=10, verbose=True):
        super(GPModelWithMean, self).__init__(kernel, noise_var, exact_feval, normalize_Y, optimizer, max_iters, optimize_restarts, sparse, num_inducing, verbose)
        self.mean_function = mean_function

    def _create_model(self, X, Y):
        # --- define kernel
        self.input_dim = X.shape[1]
        if self.kernel is None:
            kern = GPy.kern.Matern32(self.input_dim, variance=1.) #+ GPy.kern.Bias(self.input_dim)
        else:
            kern = self.kernel
            self.kernel = None

        # --- define model
        noise_var = Y.var()*0.01 if self.noise_var is None else self.noise_var

        if not self.sparse:
            self.model = GPy.models.GPRegression(X, Y, kernel=kern, mean_function=self.mean_function, noise_var=noise_var)
        else:
            self.model = GPy.models.SparseGPRegression(X, Y, kernel=kern, mean_function=self.mean_function, num_inducing=self.num_inducing)

        # --- restrict variance if exact evaluations of the objective
        if self.exact_feval:
            self.model.Gaussian_noise.constrain_fixed(1e-6, warning=False)
        else:
            self.model.Gaussian_noise.constrain_positive(warning=False)

if __name__ == '__main__':
    pass