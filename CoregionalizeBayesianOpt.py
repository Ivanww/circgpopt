
import numpy as np
import numpy.matlib as mt

from EpsilonGreedyByOpt import EpsilonGreedyBayesianOptimization
from CoregionalizedEI import CoregionalizedEI

class CoregionalizedBayesianOpt(EpsilonGreedyBayesianOptimization):
    def __init__(self, model, space, objective, acquisition, evaluator, X_init, Y_init=None, cost=None, normalize_Y=None, model_update_interval=1, epsilon=0.05, logger=None):
        super(CoregionalizedBayesianOpt, self).__init__(model, space, objective, acquisition, evaluator, X_init, Y_init, cost, normalize_Y, model_update_interval, epsilon, logger)
        self.dim = self.X.shape[1]
        self.outputNum = self.Y.shape[1]
        self.logger.info('[DIM, OUTPUT_NUM] [%d %d]' % (self.dim, self.outputNum))

    def _update_model(self):
        '''
        train data should be distribute to each output
        when updating model, replace the old one with a new one
        Note: Use exact the same data for evary output will result in some problems
        '''
        if self.notBetter == self.model_update_interval:
            self.notBetter = 0
            # distribute data
            dataSize = self.X.shape[0]
            trainSize = int(dataSize * 0.75)
            index = [np.random.choice(dataSize, trainSize, replace=False) for i in range(self.outputNum)]
            xList = [self.X[i] for i in index]
            yList = [self.Y[index[i], i].reshape(trainSize, 1) for i in range(self.outputNum)]
            # update model
            self.model.updateModel(xList, yList, None, None)
            # sync model with acquisition
            self.acquisition.model = self.model

    def _predict_result(self):
        # get predict
        m, s = self.model.predict(self.suggested_sample)
        self.logger.info('[Mean Pred]%s' % (str(m)))
        self.logger.info('[S Pred]%s' % (str(s)))
