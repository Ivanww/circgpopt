import os
import copy
import time

import numpy as np
from GPyOpt.core.task.objective import Objective

import SPReportPipeline as SPP
import ConfigLoader as CL
# from InvalidSafeObjective import InvalidSafeObjective
from Utils import feasible, validate

class SpiceObjective(Objective):
    '''evaluate by hspice'''
    def __init__(self, config, pipeline=None):
        self.config = config
        self.logger = self.config.logger

        self.cmd = self.config.hspice
        self.workdir = self.config.workdir
        self.nlfile = os.path.join(self.config.workdir, self.config.nl_filename)
        self.nlpara = os.path.join(self.config.workdir, self.config.para_filename)
        self.monitor_num = self.config.monitor_num
        self.reportPipeline = pipeline
        self.evaluationCount = 0
        if not self.reportPipeline:
            self._initPipeline()
        else:
            self.logger.debug('use predefined report pipeline')

    def _initPipeline(self):
        '''initial report pipeline'''
        self.reportPipeline = SPP.SPReportPipeline(self.config, self.logger)
        # self.reportPipeline.buildPineline(self.config)

    def _estimate(self, x):
        '''estimate a set of paras'''
        os.chdir(self.workdir)
        self.logger.debug('start evaluation: \n %s' % (str(x)))
        with open(self.nlpara, 'w') as parafile:
            for i in range(self.config.para_num):
                parafile.write('.PARAM %s = %E\n' % (self.config.para_name[i], x[i]))
        report = os.popen('%s %s' % (self.cmd, self.nlfile))
        for line in report:
            self.reportPipeline.process(line)
        result = self.reportPipeline.briefSummary()
        self.logger.debug('evaluation result : \n %s' % (str(result)))
        self.reportPipeline.resetPipeline()
        return result

    def evaluate(self, X):
        '''estimate sets of paras one by one
           for inf and nan result, do nothing
           return np.array for evals and list for cost
        '''
        f_evals = np.empty(shape=[0,self.monitor_num])
        cost_evals = np.empty(shape=(X.shape[0],))
        self.evaluationCount = 0

        for i in range(X.shape[0]):
            st_time = time.time()
            fx = self._estimate(X[i,:])
            f_evals = np.vstack((f_evals, copy.deepcopy(fx)))
            cost_evals[i] = time.time() - st_time
            if np.isinf(fx).any():
                self.logger.warning('INF RET!! %s' % (fx))
                self.logger.warning('INPUT : %s' % (X[i]))
            self.evaluationCount += 1
            if self.evaluationCount % 50 == 0:
                self.logger.info('%d samples evaluated!' % (self.evaluationCount))

        self.logger.info('Total Evaluated : %d' % (X.shape[0]))
        self.logger.info('Total Valid: %d' % (validate(f_evals)[0].shape[0]))
        self.logger.info('Total Feasible: %d' % (f_evals[feasible(f_evals)].shape[0]))
        return f_evals, cost_evals.tolist()

if __name__ == '__main__':
    import numpy as np
    from GPyOpt.util.stats import initial_design
    from GPyOpt import Design_space

    # config = CL.ConfigLoader('singleEndedConf.json')
    config = CL.ConfigLoader('twoStage.json')
    space = Design_space(space = config.space)

    initX = initial_design(config.init_sample_method, space, config.init_sample_size)
    spice = SpiceObjective(config)
    result, cost = spice.evaluate(initX)
    print(result)
    print(cost)
    for i in range(result[feasible(result)].shape[0]):
        print(result[i,:])
