
import GPy
import GPyOpt

def testBasisFunc():
    import numpy as np
    X = np.random.uniform(-3.,3.,(20,1))
    Y = 2 + np.sin(X) + np.random.randn(20,1)*0.05
    kernel = GPy.kern.RBF(input_dim=1, variance=1., lengthscale=1., ARD=True)
    kern2 = GPy.kern.RBF(1, ARD=True) + GPy.kern.Bias(1)
    m = GPy.models.GPRegression(X, Y, kernel)
    m2 = GPy.models.GPRegression(X, Y, kern2)
    m.optimize()
    m2.optimize()
    print(m.predict(np.array([[1]])))
    print(m2.predict(np.array([[1]])))


def testAcquisitionEIWithTau():
    # from numpy.random import seed
    # seed(123456)
    func  = GPyOpt.objective_examples.experiments2d.branin()
    # func.plot()
    objective = GPyOpt.core.task.SingleObjective(func.f)
    space = GPyOpt.Design_space(space =[{'name': 'var_1', 'type': 'continuous', 'domain': (-5,10)},
                                        {'name': 'var_2', 'type': 'continuous', 'domain': (1,15)}])
    model = GPyOpt.models.GPModel(kernel=GPy.kern.RBF(2,ARD=True),optimize_restarts=2,verbose=False)
    mmodel = GPyOpt.models.GPModel(kernel=GPy.kern.RBF(2,ARD=True),optimize_restarts=2,verbose=False)
    aquisition_optimizer = GPyOpt.optimization.AcquisitionOptimizer(space)
    mAcqOpt = GPyOpt.optimization.AcquisitionOptimizer(space)
    from AcquisitionEIWithTau import AcquisitionEIWithTau
    initial_design = GPyOpt.util.stats.initial_design('random', space, 5)
    acquisition = GPyOpt.acquisitions.AcquisitionEI(model, space, optimizer=aquisition_optimizer)
    mAcq = AcquisitionEIWithTau(mmodel, space, optimizer=mAcqOpt)
    evaluator = GPyOpt.core.evaluators.Sequential(acquisition)
    mevaluator = GPyOpt.core.evaluators.Sequential(acquisition)

    bo = GPyOpt.methods.ModularBayesianOptimization(model, space, objective, acquisition, evaluator, initial_design)
    fmin = acquisition.model.get_fmin()
    print(fmin)
    mAcq.setTau(fmin)
    mbo = GPyOpt.methods.ModularBayesianOptimization(mmodel, space, objective, mAcq, mevaluator, initial_design)
    mfmin = mAcq.model.get_fmin()
    print(mfmin)
    for i in range(10):
        bo.run_optimization(max_iter=1)
        fmin = acquisition.model.get_fmin()
        print(fmin)
        mAcq.setTau(fmin)
        mbo.run_optimization(max_iter=1)
        mfmin = mAcq.model.get_fmin()
        print(mfmin)
        print(bo.suggested_sample)
        print(mbo.suggested_sample)

if __name__ == '__main__':
    testBasisFunc()