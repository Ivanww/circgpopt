import copy

import numpy as np
from GPyOpt.core.task.objective import Objective
from GPyOpt.util.stats import initial_design

class InvalidSafeObjective(Objective):
    def __init__(self, space, validFunc):
        self.validate = lambda x: np.apply_along_axis(validFunc, 1, x)
        self.space = space
        self.validX = None
        self.isEvaluateSuccess = True

    def getCoreObjective(self):
        return self

    def evaluate(self, X):
        self.validX = None
        self.isEvaluateSuccess = True
        f_eval, cost_eval = self._evaluate(X)

        if self.validate(f_eval).any():
            valid_eval = f_eval[self.validate(f_eval)]
            valid_cost = cost_eval[self.validate(f_eval)]
        else:
            valid_eval = np.empty(shape=(0, f_eval.shape[1]))
            # valid_cost = np.empty(shape=(0, cost_eval.shape[1]))

        if valid_eval.shape[0] > 0:
            if valid_eval.shape[0] != X.shape[0]:
                self.isEvaluateSuccess = False
                self.validX = X[self.validate(f_eval)]
        else:
            sampleNum = X.shape[0]
            self.isEvaluateSuccess = False
            print('invalid sample! use random sample instead!')
            while (valid_eval.size == 0):
                self.validX = initial_design('random', self.space, sampleNum)
                print('[Random Sample][%s]' % (str(self.validX)))
                f_eval, cost_eval = self._evaluate(self.validX)
                valid_eval = f_eval[self.validate(f_eval)]
                valid_cost = cost_eval[self.validate(f_eval)]

        return valid_eval, valid_cost.tolist()

    def _evaluate(self, X):
       raise NotImplementedError()
