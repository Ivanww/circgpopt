
import numpy as np
from GPyOpt.util.stats import initial_design

from Utils import feasible

def initial_circuit(inputdim, sample, space, size, objective):
    X = np.empty(shape=(size, inputdim))
    est_num = 0
    for i in range(size):
        while True:
            est_num += 1
            if est_num % 100 == 0:
                print('[Eval Num, Feas Num][%d, %d]' % (est_num, i))
            np.random.seed(est_num)
            design = initial_design(sample, space, 1)
            if feasible(objective.evaluate(design)[0]).all():
                break
        print('[Feab Input] %s' % (str(design)))
        X[i] = design.flatten()
    return X

if __name__ == '__main__':
    from SPObjective import SpiceObjective
    from ConfigLoader import ConfigLoader
    from GPyOpt import Design_space

    config = ConfigLoader('singleEndedConf.json')
    space = Design_space(space = config.space)
    objective = SpiceObjective(config, space)

    X = initial_circuit(config.para_num, 'random', space, 200, objective)
    print(X)
    np.savetxt('input.txt', X)